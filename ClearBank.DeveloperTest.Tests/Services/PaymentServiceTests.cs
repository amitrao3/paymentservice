﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ClearBank.DeveloperTest.Services;
using ClearBank.DeveloperTest.Repository;
using ClearBank.DeveloperTest.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;

namespace ClearBank.DeveloperTest.Tests.Services
{
    [TestClass()]
    public class PaymentServiceTests
    {
        private Mock<IPaymentRepository> _mockPaymentRepository;
        private IPaymentService _paymentService;
        private MakePaymentRequest _request;

        [TestInitialize]
        public void Initialise()
        {
            _mockPaymentRepository = new Mock<IPaymentRepository>();
            _paymentService = new PaymentService(_mockPaymentRepository.Object);
        }

        [TestMethod()]
        public void WhenExceptionOccursInService_ShouldReturnFalse()
        {
            _request = new MakePaymentRequest();
            _mockPaymentRepository.Setup(x=> x.ExecutePayment(_request.DebtorAccountNumber,_request.Amount,_request.PaymentScheme))
                                        .Throws(new Exception());
            var result = _paymentService.MakePayment(_request);
            Assert.AreEqual(false, result.Success);
        }

        [TestMethod()]
        public void WhenValidRequestAndSuccess_TShouldReturnTrue()
        {
            _request = new MakePaymentRequest(){Amount=10M, CreditorAccountNumber="A12000", DebtorAccountNumber= "A12000", PaymentDate=DateTime.Now, PaymentScheme=PaymentScheme.Bacs };
            _mockPaymentRepository.Setup(x => x.ExecutePayment("A12000", 10M, PaymentScheme.Bacs)).Returns(true);
            var result = _paymentService.MakePayment(_request);
            Assert.AreEqual(true, result.Success);
        }

        [TestMethod]
        public void WhenValidRequestAndFail_ShouldReturnFalse()
        {
            _request = new MakePaymentRequest() { Amount = 10M, CreditorAccountNumber = "A12000", DebtorAccountNumber = "A12000", PaymentDate = DateTime.Now, PaymentScheme = PaymentScheme.Bacs };
            _mockPaymentRepository.Setup(x => x.ExecutePayment(_request.DebtorAccountNumber, _request.Amount, _request.PaymentScheme)).Returns(false);
            var result = _paymentService.MakePayment(_request);
            Assert.AreEqual(false, result.Success);
        }
    }
}