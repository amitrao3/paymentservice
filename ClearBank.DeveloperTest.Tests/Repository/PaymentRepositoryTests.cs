﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ClearBank.DeveloperTest.Repository;
using ClearBank.DeveloperTest.Data.Interface;
using ClearBank.DeveloperTest.Data.Implementation;
using ClearBank.DeveloperTest.Types;


namespace ClearBank.DeveloperTest.Tests.Repository
{
    [TestClass()]
    public class PaymentRepositoryTests
    {
        private Mock<IDataStore> _mockDatastore;
        private IPaymentRepository _paymentRepository;
        private Decimal _balance;
        private Decimal _zeroBalance;
        private Decimal _amountLessThanBalance;
        private Decimal _amountMoreThanBalance;
        private Decimal _amountEqualToBalance;
        private string _debtorAccountNumber;
        private string _notexistentAccountNumber;
        private AccountStatus _activeStatus;
        private AccountStatus _inactiveStatus;
        private PaymentScheme _bacsPaymentScheme;
        private PaymentScheme _chapsPaymentScheme;
        private PaymentScheme _fasterPaymentScheme;
        private Account _account;

        [TestInitialize]
        public void Initialize()
        {
            _balance = 100M;
            _zeroBalance = 0M;
            _amountEqualToBalance = _balance;
            _amountLessThanBalance = _balance - 10M;
            _amountMoreThanBalance = _balance + 50M;
            _debtorAccountNumber = "A12000";
            _notexistentAccountNumber = "Z99999";
            _activeStatus = AccountStatus.Live;
            _inactiveStatus = AccountStatus.Disabled;
            _bacsPaymentScheme = PaymentScheme.Bacs;
            _chapsPaymentScheme = PaymentScheme.Chaps;
            _fasterPaymentScheme = PaymentScheme.FasterPayments;
            _mockDatastore = new Moq.Mock<IDataStore>();
            _paymentRepository = new PaymentRepository(_mockDatastore.Object);
            _account = new Account() { AccountNumber = _debtorAccountNumber,
                                       Balance = _balance,
                                       AllowedPaymentSchemes = AllowedPaymentSchemes.Bacs,
                                       Status = _activeStatus };
        }

        [TestMethod]
        public void WhenAccountDoesNotExist_ThenShouldReturnFalse()
        {
            _mockDatastore.Setup(x => x.GetAccount(_debtorAccountNumber)).Returns(_account);
            var result = _paymentRepository.ExecutePayment(_notexistentAccountNumber, _amountLessThanBalance, _bacsPaymentScheme);
            Assert.AreEqual(false, result);
        }

        #region Bacs Account
        [TestMethod]
        public void WhenBacsAccountExistsAndBacsPaymentWithSufficientBalance_ThenShouldReturnTrue()
        {
            _mockDatastore.Setup(x => x.GetAccount(_debtorAccountNumber)).Returns(_account);
            var result = _paymentRepository.ExecutePayment(_debtorAccountNumber, _amountLessThanBalance, _bacsPaymentScheme);
            Assert.AreEqual(true, result);
        }

        [TestMethod]
        public void WhenBacsAccountExistsAndBacsPaymentWithInSufficientBalance_ThenShouldReturnTrue()
        {
            _mockDatastore.Setup(x => x.GetAccount(_debtorAccountNumber)).Returns(_account);
            var result = _paymentRepository.ExecutePayment(_debtorAccountNumber, _amountMoreThanBalance, _bacsPaymentScheme);
            Assert.AreEqual(true, result);
        }

        [TestMethod]
        public void WhenBacsAccountExistsAndChapsPayment_ThenShouldReturnFalse()
        {
            _mockDatastore.Setup(x => x.GetAccount(_debtorAccountNumber)).Returns(_account);
            var result = _paymentRepository.ExecutePayment(_debtorAccountNumber, _amountMoreThanBalance, _chapsPaymentScheme);
            Assert.AreEqual(false, result);
        }

        [TestMethod]
        public void WhenBacsAccountExistsAndStatusIsDisabledAndBacsPaymentWithSufficientBalance_ThenShouldReturnTrue()
        {
            Account disabledAccount = new Account()
                                        { AccountNumber = _debtorAccountNumber,
                                          Balance = _balance,
                                          AllowedPaymentSchemes = AllowedPaymentSchemes.Bacs,
                                          Status = _inactiveStatus };
            _mockDatastore.Setup(x => x.GetAccount(_debtorAccountNumber)).Returns(disabledAccount);
            var result = _paymentRepository.ExecutePayment(_debtorAccountNumber, _amountLessThanBalance, _bacsPaymentScheme);
            Assert.AreEqual(true, result);
        }

        [TestMethod]
        public void WhenBacsAccountExistsAndFasterPayment_ThenShouldReturnFalse()
        {
            _mockDatastore.Setup(x => x.GetAccount(_debtorAccountNumber)).Returns(_account);
            var result = _paymentRepository.ExecutePayment(_debtorAccountNumber, _amountMoreThanBalance, _fasterPaymentScheme);
            Assert.AreEqual(false, result);
        }

        #endregion
        #region Faster payments
        [TestMethod]
        public void WhenFasterPaymentsAccountExistsAndFasterPaymentWithSufficientBalance_ThenShouldReturnTrue()
        {
            Account fasterPaymentAccount = new Account()
            {
                AccountNumber = _debtorAccountNumber,
                Balance = _balance,
                AllowedPaymentSchemes = AllowedPaymentSchemes.FasterPayments,
                Status = _activeStatus
            };
            _mockDatastore.Setup(x => x.GetAccount(_debtorAccountNumber)).Returns(fasterPaymentAccount);
            var result = _paymentRepository.ExecutePayment(_debtorAccountNumber, _amountLessThanBalance, _fasterPaymentScheme);
            Assert.AreEqual(true, result);
        }

        [TestMethod]
        public void WhenFasterPaymentsAccountExistsAndFasterPaymentWithInSufficientBalance_ThenShouldReturnFalse()
        {
            Account fasterPaymentAccount = new Account()
            {
                AccountNumber = _debtorAccountNumber,
                Balance = _balance,
                AllowedPaymentSchemes = AllowedPaymentSchemes.FasterPayments,
                Status = _activeStatus
            };
            _mockDatastore.Setup(x => x.GetAccount(_debtorAccountNumber)).Returns(fasterPaymentAccount);
            var result = _paymentRepository.ExecutePayment(_debtorAccountNumber, _amountMoreThanBalance, _fasterPaymentScheme);
            Assert.AreEqual(false, result);
        }
        [TestMethod]
        public void WhenFasterPaymentsAccountExistsAndBacsPaymentWithSufficientBalance_ThenShouldReturnFalse()
        {
            Account fasterPaymentAccount = new Account()
            {
                AccountNumber = _debtorAccountNumber,
                Balance = _balance,
                AllowedPaymentSchemes = AllowedPaymentSchemes.FasterPayments,
                Status = _activeStatus
            };
            _mockDatastore.Setup(x => x.GetAccount(_debtorAccountNumber)).Returns(fasterPaymentAccount);
            var result = _paymentRepository.ExecutePayment(_debtorAccountNumber, _amountLessThanBalance, _bacsPaymentScheme);
            Assert.AreEqual(false, result);
        }

        #endregion
        #region Chaps
        [TestMethod]
        public void WhenChapsAccountExistsAndIsLiveChapsWithSufficientBalance_ThenShouldReturnTrue()
        {
            Account chapsPaymentAccount = new Account()
            {
                AccountNumber = _debtorAccountNumber,
                Balance = _balance,
                AllowedPaymentSchemes = AllowedPaymentSchemes.Chaps,
                Status = _activeStatus
            };
            _mockDatastore.Setup(x => x.GetAccount(_debtorAccountNumber)).Returns(chapsPaymentAccount);
            var result = _paymentRepository.ExecutePayment(_debtorAccountNumber, _amountLessThanBalance, _chapsPaymentScheme);
            Assert.AreEqual(true, result);
        }
        [TestMethod]
        public void WhenChapsAccountExistsAndIsNotLiveChapsWithSufficientBalance_ThenShouldReturnFalse()
        {
            Account chapsPaymentAccount = new Account()
            {
                AccountNumber = _debtorAccountNumber,
                Balance = _balance,
                AllowedPaymentSchemes = AllowedPaymentSchemes.Chaps,
                Status = _inactiveStatus
            };
            _mockDatastore.Setup(x => x.GetAccount(_debtorAccountNumber)).Returns(chapsPaymentAccount);
            var result = _paymentRepository.ExecutePayment(_debtorAccountNumber, _amountLessThanBalance, _chapsPaymentScheme);
            Assert.AreEqual(false, result);
        }

        [TestMethod]
        public void WhenChapsAccountExistsAndIsActiveAndBacsPaymentWithSufficientBalance_ThenShouldReturnFalse()
        {
            Account chapsPaymentAccount = new Account()
            {
                AccountNumber = _debtorAccountNumber,
                Balance = _balance,
                AllowedPaymentSchemes = AllowedPaymentSchemes.Chaps,
                Status = _activeStatus
            };
            _mockDatastore.Setup(x => x.GetAccount(_debtorAccountNumber)).Returns(chapsPaymentAccount);
            var result = _paymentRepository.ExecutePayment(_debtorAccountNumber, _amountLessThanBalance, _bacsPaymentScheme);
            Assert.AreEqual(false, result);
        }

        #endregion


    }
}