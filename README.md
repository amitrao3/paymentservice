# PaymentService

Sample project with the following functionality

1. Lookup the account the payment is being made from.
2. Verify that the payment can be withdrawn from the account.
3. Deduct the payment amount from the account�s balance and update the account in the database.

Project comprises of:

a. PaymentService - Main class containing public methods for client use.

b. PaymentRepository - Contains business logic.

c. Datastore classes (AccountDataStore , BackupAccountDataStore) - data retrieving classes.
	
ClearBank.DeveloperTest.Tests contains unit tests using VisualStudio TestTools and mocking framework Moq .