﻿using System;
using ClearBank.DeveloperTest.Data.Implementation;
using ClearBank.DeveloperTest.Data.Interface;
using ClearBank.DeveloperTest.Repository;
using ClearBank.DeveloperTest.Types;
using System.Configuration;

namespace ClearBank.DeveloperTest.Services
{
    public class PaymentService : IPaymentService
    {
        private readonly IPaymentRepository _paymentRepository;

        public PaymentService()
        {
            _paymentRepository = new PaymentRepository();
        }

        public PaymentService(IPaymentRepository paymentRepository) => _paymentRepository = paymentRepository;
        

        public MakePaymentResult MakePayment(MakePaymentRequest request)
        {
            var result = new MakePaymentResult();
            try
            {
                result.Success = _paymentRepository.ExecutePayment(request.DebtorAccountNumber, request.Amount, request.PaymentScheme);
                return result;
            }
            catch(Exception exception)
            {
                // Record the exception in logs
                result.Success = false;
                return result;
            }
        }
    }
}
