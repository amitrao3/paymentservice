﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClearBank.DeveloperTest.Types;

namespace ClearBank.DeveloperTest.Repository
{
    public interface IPaymentRepository
    {
        bool ExecutePayment(string accountNumber, decimal amount, PaymentScheme paymentScheme);
    }
}
