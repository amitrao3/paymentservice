﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using ClearBank.DeveloperTest.Types;
using ClearBank.DeveloperTest.Data.Interface;
using ClearBank.DeveloperTest.Data.Implementation;

namespace ClearBank.DeveloperTest.Repository
{
    public class PaymentRepository: IPaymentRepository
    {
        private IDataStore _dataStore;
        private Account _account;
 
        public PaymentRepository(IDataStore dataStore = null)
        {
            if (dataStore is null)
            {
                var dataStoreType = ConfigurationManager.AppSettings["DataStoreType"];
                if (dataStoreType == "Backup")
                    _dataStore = new BackupAccountDataStore();
                else
                    _dataStore = new AccountDataStore();
            }
            else
            { _dataStore = dataStore; }
        }

        public bool ExecutePayment(string accountNumber, decimal amount, PaymentScheme paymentScheme)
        {
            bool isSuccess = false;            
            _account = _dataStore.GetAccount(accountNumber);
            var allowPayment = ValidatePayment(paymentScheme, amount);
            if (allowPayment)
            {
                _account.Balance -= amount;
                _dataStore.UpdateAccount(_account);
                isSuccess = true;
            }
            return isSuccess;
        }

        private bool ValidatePayment(PaymentScheme paymentScheme, Decimal amount)
        {
            bool isValid = false;
            if (_account == null) return isValid;
            switch (paymentScheme)
            {
                case PaymentScheme.Bacs:
                    if (_account.AllowedPaymentSchemes.HasFlag(AllowedPaymentSchemes.Bacs))
                    {
                        isValid = true;
                    }
                    break;

                case PaymentScheme.FasterPayments:
                    if (_account.AllowedPaymentSchemes.HasFlag(AllowedPaymentSchemes.FasterPayments) && _account.Balance >= amount)
                    {
                        isValid = true;
                    }
                    break;

                case PaymentScheme.Chaps:
                    if (_account.AllowedPaymentSchemes.HasFlag(AllowedPaymentSchemes.Chaps) && (_account.Status == AccountStatus.Live))
                    {
                        isValid = true;
                    }
                    break;
            }
            return isValid;
        }
    }
}
