﻿using ClearBank.DeveloperTest.Types;

namespace ClearBank.DeveloperTest.Data.Interface
{
    public interface IDataStore
    {
        Account GetAccount(string accountNumber);

        // Ideally the update method would return a boolean status code 
        void UpdateAccount(Account account);
    }
}